import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
//   selector -> name we fgive the Angualr component when it is rendered as an
// HTML element on the page (app- component name)
  selector: 'app-product-alerts',
  templateUrl: './product-alerts.component.html',
  styleUrls: ['./product-alerts.component.css']
})
export class ProductAlertsComponent implements OnInit {

  // @Input indicates that the property value passes in from the component's parent,
  // the product list component
  @Input() product;

  // allows the product alert component to emit an event when the value of the nofify
  // property changes (button - nofiy.emit())
  @Output() notify = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
